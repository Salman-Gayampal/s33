async function retrieveTodos() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos');
    const jsonOne = await response.json();
    console.log(jsonOne);
  
    const returnTitle = jsonOne.map((json) => 'title: ' + json.title);
    console.log(returnTitle);

}
retrieveTodos();

fetch('https://jsonplaceholder.typicode.com/todos/57')
    .then((res) => res.json())
    .then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/57')
    .then((res) => res.json())
    .then((json) => {
        console.log(`title: ${json.title} | completed: ${json.completed}`);
    });


async function createTodo() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos',
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                userId: 097,
                completed: false,
                title: "Fly High Butterfly"
            })
        });

    const jsonOne = await response.json();
    console.log(jsonOne);
}
createTodo();


async function updateTodo() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/57',
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                title: "Do laundry, maybe"
            })
        });

    const jsonOne = await response.json();
    console.log(jsonOne);
}
updateTodo();

async function updateTodoStructure() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/57',
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                title: "Run Forrest, run!",
                description: "Run like a tiger",
                status: "Incomplete",
                date_completed: 'Aug. 26, 2022',
                userId: 03523
            })
        });

    const jsonOne = await response.json();
    console.log(jsonOne);
}
updateTodoStructure()

async function patchTodo() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/57',
        {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                title: "Eat Fish",
            })
        });

    const jsonOne = await response.json();
    console.log(jsonOne);
}
patchTodo();

async function completeTodo() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/57',
        {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                completed: true,
                date_completed: 'Aug. 26, 2022'
            })
        });

    const jsonOne = await response.json();
    console.log(jsonOne);
}
completeTodo();

async function removeTodo() {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos/57',
        {
            method: "DELETE"
        });

    const jsonOne = await response.json();
    console.log(jsonOne);
}
removeTodo();